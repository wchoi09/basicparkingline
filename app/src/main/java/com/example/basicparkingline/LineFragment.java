package com.example.basicparkingline;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.AspectRatio;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.basicparkingline.databinding.FragmentLineBinding;
import com.google.common.util.concurrent.ListenableFuture;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class LineFragment extends Fragment implements View.OnClickListener {

    public LineFragment() {
        // Required empty public constructor
    }

    private FragmentLineBinding binding;
    private Button mResetCoordinatesBtn;
    private Button mTakeCaptureBtn;
    private Button mMoveLeftBtn;
    private Button mMoveUpBtn;
    private Button mMoveRightBtn;
    private Button mMoveDownBtn;
    private ProcessCameraProvider cameraProvider;

    private static final int REQUEST_CAMERA_PERMISSION = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentLineBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mResetCoordinatesBtn = binding.btnResetCoordinates;
        mResetCoordinatesBtn.setOnClickListener(this);
        mTakeCaptureBtn = binding.btnTakeCapture;
        mTakeCaptureBtn.setOnClickListener(this);
        mMoveLeftBtn = binding.btnLeft;
        mMoveLeftBtn.setOnClickListener(this);
        mMoveUpBtn = binding.btnUp;
        mMoveUpBtn.setOnClickListener(this);
        mMoveRightBtn = binding.btnRight;
        mMoveRightBtn.setOnClickListener(this);
        mMoveDownBtn = binding.btnDown;
        mMoveDownBtn.setOnClickListener(this);

        // Get the PreviewView from the layout
        PreviewView previewView = binding.previewView;
        binding.previewView.post(this::setUpCamera);
        if (hasPermission()) {
            navigateToCamera();
            // bindCameraUseCases();
        } else {
            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;

        // Get the ProcessCameraProvider and unbind all use cases
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture =
                ProcessCameraProvider.getInstance(requireContext());
        cameraProviderFuture.addListener(
                () -> {
                    try {
                        ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                        cameraProvider.unbindAll();
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                },
                ContextCompat.getMainExecutor(requireContext()));
    }

    @Override
    public void onClick(View v) {
        int vId = v.getId();

        if (vId == R.id.btn_left) {
            binding.viewLine.moveSelectedPoint(0, 1);
        } else if (vId == R.id.btn_up) {
            binding.viewLine.moveSelectedPoint(1, 1);
        } else if (vId == R.id.btn_right) {
            binding.viewLine.moveSelectedPoint(2, 1);
        } else if (vId == R.id.btn_down) {
            binding.viewLine.moveSelectedPoint(3, 1);
        }

        if (vId == R.id.btn_reset_coordinates) {
            binding.viewLine.initPoints();
            Toast.makeText(getActivity(), String.format("RESET"), Toast.LENGTH_SHORT).show();
        } else if (vId == R.id.btn_take_capture) {
            Toast.makeText(getActivity(), String.format("CAPTURE"), Toast.LENGTH_SHORT).show();
            // Camera view
            Bitmap bm = binding.previewView.getBitmap();
            Bitmap lineBitmap = binding.viewLine.takeScreenshot();

            Canvas canvas = new Canvas(bm);
            canvas.drawBitmap(bm, 0, 0, null);
            canvas.drawBitmap(lineBitmap, 0, 0, null);

            // Create a file object to save the screenshot to
            File screenshotsDirectory =
                    requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            String screenshotFilename = "screenshot_" + System.currentTimeMillis() + ".png";
            File screenshotFile = new File(screenshotsDirectory, screenshotFilename);

            // Save the screenshot to the file
            try (FileOutputStream outputStream = new FileOutputStream(screenshotFile)) {
                bm.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                Toast.makeText(
                                requireContext(),
                                "Screenshot saved to " + screenshotFile.getAbsolutePath(),
                                Toast.LENGTH_SHORT)
                        .show();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(requireContext(), "Failed to save screenshot", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private boolean hasPermission() {
        return ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(
                    new ActivityResultContracts.RequestPermission(),
                    isGranted -> {
                        if (isGranted) {
                            Toast.makeText(
                                            requireContext(),
                                            "Permission request granted",
                                            Toast.LENGTH_LONG)
                                    .show();
                            navigateToCamera();
                        } else {
                            Toast.makeText(
                                            requireContext(),
                                            "Permission request denied",
                                            Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

    // Initialize CameraX, and prepare to bind the camera use cases
    private void setUpCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture =
                ProcessCameraProvider.getInstance(requireContext());
        cameraProviderFuture.addListener(
                () -> {
                    try {
                        cameraProvider = cameraProviderFuture.get();

                        // Build and bind the camera use cases
                        bindCameraUseCases();
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                },
                ContextCompat.getMainExecutor(requireContext()));
    }

    private void bindCameraUseCases() {
        // CameraSelector - makes assumption that we're only using the back
        // camera
        CameraSelector.Builder cameraSelectorBuilder = new CameraSelector.Builder();
        CameraSelector cameraSelector =
                cameraSelectorBuilder.requireLensFacing(CameraSelector.LENS_FACING_BACK).build();

        // Preview. Only using the 4:3 ratio because this is the closest to
        // our model
        Preview preview = new Preview.Builder().setTargetAspectRatio(AspectRatio.RATIO_4_3).build();
        preview.setSurfaceProvider(binding.previewView.getSurfaceProvider());

        // Must unbind the use-cases before rebinding them
        // cameraProvider.unbindAll();
        cameraProvider.bindToLifecycle(this, cameraSelector, preview);
    }

    private void navigateToCamera() {}
}
