package com.example.basicparkingline;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import androidx.camera.view.PreviewView;

import java.util.ArrayList;
import java.util.List;

public class LineView extends View {

    private List<PointF> mPoints;
    private PointF mLeftTopPoint;
    private PointF mRightTopPoint;
    private PointF mLeftBottomPoint;
    private PointF mRightBottomPoint;
    private Path mPath = new Path();
    private Paint mLinePaint;
    private int mWidth;
    private int mHeight;

    public LineView(Context context) {
        super(context);
        init(context);
    }

    public LineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LineView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        setBackgroundColor(Color.TRANSPARENT);
        initPoints();
        mLinePaint = new Paint();
        mLinePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mLinePaint.setStrokeWidth(6f);
        mLinePaint.setColor(Color.BLUE);
        mLinePaint.setStyle(Paint.Style.STROKE);
    }

    public void initPoints() {
        mLeftTopPoint = new PointF();
        mLeftTopPoint.set(mWidth * 0.4f, mHeight * 0.4f);

        mRightTopPoint = new PointF();
        mRightTopPoint.set(mWidth * 0.6f, mHeight * 0.4f);

        mLeftBottomPoint = new PointF();
        mLeftBottomPoint.set(mWidth * 0.4f, mHeight * 0.6f);

        mRightBottomPoint = new PointF();
        mRightBottomPoint.set(mWidth * 0.6f, mHeight * 0.6f);

        mPoints = new ArrayList<>();
        mPoints.add(mLeftTopPoint);
        mPoints.add(mRightTopPoint);
        mPoints.add(mLeftBottomPoint);
        mPoints.add(mRightBottomPoint);

        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        initPoints();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Green lines
        mPath.reset();
        mPath.moveTo(mLeftTopPoint.x, mLeftTopPoint.y);
        mPath.lineTo(mRightTopPoint.x, mRightTopPoint.y);
        mPath.moveTo(mRightTopPoint.x, mRightTopPoint.y);
        mPath.close();

        mLinePaint.setColor(Color.GREEN);
        canvas.drawPath(mPath, mLinePaint);

        // Yellow lines
        mPath.reset();
        mPath.moveTo(mRightTopPoint.x, mRightTopPoint.y);
        mPath.lineTo(mRightBottomPoint.x, mRightBottomPoint.y);
        mPath.moveTo(mRightBottomPoint.x, mRightBottomPoint.y);
        mPath.close();

        mLinePaint.setColor(Color.YELLOW);
        canvas.drawPath(mPath, mLinePaint);

        // Red Lines
        mPath.reset();
        mPath.moveTo(mLeftBottomPoint.x, mLeftBottomPoint.y);
        mPath.lineTo(mLeftTopPoint.x, mLeftTopPoint.y);
        mPath.moveTo(mLeftTopPoint.x, mLeftTopPoint.y);
        mPath.close();

        mLinePaint.setColor(Color.RED);
        canvas.drawPath(mPath, mLinePaint);
    }

    public void moveSelectedPoint(int dirIdx, int pixelSize) {
        PointF point;

        if (dirIdx == 0) {
            for (int i = 0; i < mPoints.size(); i++) {
                point = mPoints.get(i);
                point.x -= pixelSize;
                mPoints.set(i, point);
            }
        } else if (dirIdx == 1) {
            for (int i = 0; i < mPoints.size(); i++) {
                point = mPoints.get(i);
                point.y -= pixelSize;
                mPoints.set(i, point);
            }
        } else if (dirIdx == 2) {
            for (int i = 0; i < mPoints.size(); i++) {
                point = mPoints.get(i);
                point.x += pixelSize;
                mPoints.set(i, point);
            }
        } else if (dirIdx == 3) {
            for (int i = 0; i < mPoints.size(); i++) {
                point = mPoints.get(i);
                point.y += pixelSize;
                mPoints.set(i, point);
            }
        }
        invalidate();
    }

    public Bitmap takeScreenshot() {
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        layout(getLeft(), getTop(), getRight(), getBottom());

        // Draw the camera preview onto the canvas
        PreviewView previewView = ((ViewGroup) getParent()).findViewById(R.id.previewView);
        previewView.draw(canvas);

        // Draw the paths onto the canvas
        draw(canvas);

        return bitmap;
    }
}
